# reward-bot

## Running the bot

### Pre-requisites
Docker, Docker Compose

### Setup
Add the following into a file named `bot.env` in the root:
 - SLACK_BOT_TOKEN=<your bot token>
 - PRIVILEGED_USERS=<comma-separated list of usernames without @ signs, eg. prox,firs,frank>
 - (optional) PROD=1 (to run as production; to run as test, remove this key from bot.env completely.

When your config is set, run `docker-compose up -d --build` to run as a daemon process within Docker.

To replenish vouchers:
 - ensure that the `bot` container is not running (`docker-compose down`)
 - change the voucher file in `data/vouchers.txt`, one voucher code per-line
 - rebuild the `bot` container (`docker-compose build bot`)
 - run the task `docker-compose run bot replenish`
 - bring the bot up again (`docker-compose up -d`)

## Usage/Development
Send a message to the bot that is running to be told what actions it can understand.

These commands are specified in `app/commands.js`, with the signature:

```
{
  'title': '<text which is displayed by the bot>',
  'example': '<helper text displayed by the bot>',
  'pattern': <regex which, when matched, calls the action>,
  'action': <method name to call>
}
```

The method called is held within `app/actions` directory, so, if there is an entry for `myAwesomeAction` in `app/commands.js`, the file `myAwesomeAction.js` is what is called from within `app/actions`.

