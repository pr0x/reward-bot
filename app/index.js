const Slack = require("@slack/client");
const SLACK_BOT_TOKEN = process.env.SLACK_BOT_TOKEN || "";

const MongoClient = require("mongodb").MongoClient;
const MongoUrl = "mongodb://mongo/" + require("./config").db;

if (SLACK_BOT_TOKEN === "") {
  throw new Error(
    "You need to pass a valid Slack Bot Token (starts 'xoxb-'), eg. `$ SLACK_BOT_TOKEN=xoxb... npm start`"
  );
}

const bot = {
  web: new Slack.WebClient(SLACK_BOT_TOKEN),
  rtm: new Slack.RtmClient(SLACK_BOT_TOKEN, { useRtmConnect: true }),
  events: Slack.RTM_EVENTS,
  privilegedUsers: [],
  initiated: false
};

bot.rtm.on(Slack.CLIENT_EVENTS.RTM.AUTHENTICATED, rtmStartData => {
  if (bot.initiated) {
    console.log(`Bot already instantiated, not going to reinstantiate.`);
    return;
  }

  console.log(
    `Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}`
  );

  if (process.env.PRIVILEGED_USER_IDS) {
    bot.privilegedUsers = process.env.PRIVILEGED_USER_IDS.split(",");
  }
  
  bot.limit = process.env.LIMIT || require("../config").limit;
  bot.id = rtmStartData.self.id;

  MongoClient.connect(MongoUrl, (err, db) => {
    console.log(`Connected correctly to Mongo Server on ${MongoUrl}`);
    require("./conversation")(bot, db);
    bot.initiated = true;
  });
});

bot.rtm.on(Slack.CLIENT_EVENTS.RTM.ATTEMPTING_RECONNECT, data => {
  console.log(`[RECONNECT]: ${data}`);
});

bot.rtm.start();
