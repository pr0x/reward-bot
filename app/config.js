module.exports = {
  db: process.env.PROD ? "reward-bot" : "reward-bot-dev",
  limit: process.env.PROD ? 2 : 5
};
