const getIndividualRewards = (params, bot, db) => {
  let [user] = params;
  return new Promise((resolve, reject) => {
    db
      .collection("vouchers")
      .find({
        $or: [{ "issued.requester": user }, { "issued.recipient": user }]
      })
      .toArray()
      .then(rewards => {
        let reformatted = rewards.map(reward => {
          let d = new Date(reward.stamp);
          let dateString = [
            d.getDate(),
            d.getMonth() + 1,
            d.getFullYear()
          ].join("/");
          let subject =
            reward.issued.recipient === user
              ? `Received From: <@${reward.issued.requester}> (to you)`
              : `Given To: <@${reward.issued.recipient}> (by you)`;

          let returnedMessage = `${subject} on ${dateString} ${
            reward.issued.reason
          }`;
          if (reward.issued.recipient === user) {
            returnedMessage = [returnedMessage, `${reward.code}`].join(" - ");
          }
          return returnedMessage;
        });

        if (!reformatted.length) {
          reformatted = "You haven't given or received any rewards yet.";
        } else {
          reformatted.unshift(
            "Here are all the rewards you've given or received."
          );
        }

        resolve(reformatted);
      })
      .catch(err => {
        reject({
          type: "error",
          text: "Problem with the database, sorry."
        });

        return;
      });
  });
};

module.exports = getIndividualRewards;
