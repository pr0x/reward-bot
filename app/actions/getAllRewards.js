const getAllRewards = (params, bot, db) => {
  return new Promise((resolve, reject) => {
    db
      .collection("vouchers")
      .find({ issued: { $exists: true } })
      .sort({ stamp: -1 })
      .toArray()
      .then(rewards => {
        let reformatted = rewards.map(reward => {
          let d = new Date(reward.stamp);
          let dateString = [
            d.getDate(),
            d.getMonth() + 1,
            d.getFullYear()
          ].join("/");
          let subject = `From: <@${reward.issued.requester}> to <@${
            reward.issued.recipient
          }>`;

          return `${subject} on ${dateString} ${reward.issued.reason} - ${
            reward.code
          }`;
        });

        if (!reformatted.length) {
          reformatted = "No rewards have been given yet.";
        } else {
          reformatted.unshift("Here are all the rewards given.");
        }

        resolve(reformatted);
      })
      .catch(err => {
        reject({
          type: "error",
          text: `Problem with the database, sorry: ${err}`
        });

        return;
      });
  });
};

module.exports = getAllRewards;
