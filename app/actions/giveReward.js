const giveReward = (params, bot, db) => {
  const _isUserHuman = (id, bot) => {
    return new Promise((resolve, reject) => {
      bot.web.users
        .info(id)
        .then(user => {
          if (user.user.is_bot || user.user.id === "USLACKBOT") {
            reject();
            return;
          }
          resolve();
        })
        .catch(err => {
          console.log("ERROR");
          reject(err);
        });
    });
  };

  const _getNumberOfGivenRewardsInLastYear = requester => {
    return new Promise((resolve, reject) => {
      let d = new Date();
      d.setFullYear(d.getFullYear() - 1);

      db
        .collection("vouchers")
        .find({ "issued.requester": requester, stamp: { $gte: d } })
        .toArray()
        .then(given => {
          resolve(given.length);
        })
        .catch(err => {
          reject({
            type: "error",
            text: "Problem with the database, sorry."
          });

          return;
        });
    });
  };

  const _areRewardsAvailable = () => {
    return new Promise((resolve, reject) => {
      db
        .collection("vouchers")
        .find({ issued: { $exists: false } })
        .toArray()
        .then(vouchers => {
          if (vouchers.length) {
            resolve();
          }
          reject({
            type: "sorry",
            text:
              "No more vouchers left, sorry: the people who need to replenish the vouchers are aware."
          });

          return;
        })
        .catch(err => {
          reject({
            type: "error",
            text: "Problem with the database, sorry."
          });

          return;
        });
    });
  };

  const _saveReward = (params, bot) => {
    let [requester, recipient, reason] = params;

    return new Promise((resolve, reject) => {
      db
        .collection("vouchers")
        .findOneAndUpdate(
          { issued: { $exists: false } },
          {
            $set: {
              issued: {
                recipient: recipient,
                requester: requester,
                reason: reason
              }
            },
            $currentDate: { stamp: true }
          }
        )
        .then(r => {
          resolve(r.value.code);
        })
        .catch(err => {
          reject({
            type: "error",
            text: "Problem with the database, sorry."
          });

          return;
        });
    });
  };

  const _informRecipientAndPrivilegedUsers = (params, bot, code) => {
    let [requester, recipient, reason] = params;
    return new Promise((resolve, reject) => {
      let users = [recipient, ...bot.privilegedUsers];
      for (user of users) {
        bot.web.im.open(user).then(ret => {
          let ch = ret.channel.id;
          if (user === recipient) {
            bot.rtm.sendMessage(
              `Great news! Your colleague <@${requester}> has decided that you are deserving of a reward.  Here's what they had to say:`,
              ch
            );
            bot.rtm.sendMessage(`${reason}`, ch);
            bot.rtm.sendMessage(
              `Here is an Amazon UK voucher for you: ${code}`,
              ch
            );
            bot.rtm.sendMessage(`Enjoy!`, ch);
          } else {
            bot.rtm.sendMessage(
              `A voucher has been given by <@${requester}> to <@${recipient}>:`,
              ch
            );
            bot.rtm.sendMessage(`${reason}`, ch);
            bot.rtm.sendMessage(`${code}`, ch);
          }

          resolve();
        });
      }
    }).catch(err => {
      reject({
        type: "error",
        text: "Problem giving voucher to recipient."
      });

      return;
    });
  };

  const _informIfStockLevelsAreLow = bot => {
    const lowStockLevel = 3;

    return new Promise((resolve, reject) => {
      db
        .collection("vouchers")
        .find({ issued: { $exists: false } })
        .toArray()
        .then(vouchers => {
          if (vouchers.length <= lowStockLevel) {
            for (user of bot.privilegedUsers) {
              bot.web.im.open(user).then(ret => {
                let ch = ret.channel.id;
                bot.rtm.sendMessage(
                  `Looks like the vouchers are running low, there are ${
                    vouchers.length
                  } left.`,
                  ch
                );
              });
            }
          }
          resolve();
          return;
        })
        .catch(err => {
          reject({
            type: "error",
            text: "Problem with the database, sorry."
          });

          return;
        });
    });
  };

  return new Promise((resolve, reject) => {
    _areRewardsAvailable()
      .then(() => {
        let [requester, recipient, reason] = params;
        console.log(
          `[Reward Requested]: FROM: ${requester} | TO: ${recipient} | REASON: ${reason}`
        );

        if (requester === recipient) {
          reject({
            type: "naughty",
            text: "You can't reward yourself."
          });

          return;
        }

        if (reason.length < 32) {
          reject({
            type: "lacking",
            text: "You need to give a bit more of a reason than that."
          });

          return;
        }

        _getNumberOfGivenRewardsInLastYear(requester)
          .then(given => {
            if (
							(bot.limit > 0) &&
              (
								(given >= bot.limit) &&
                (bot.privilegedUsers.includes(requester) === false)
							)
            ) {
              reject({
                type: "error",
                text: `You can only give ${bot.limit} rewards a year.`
              });

              return;
            }

            _isUserHuman(recipient, bot)
              .then(() => _saveReward(params, bot))
              .then(code =>
                _informRecipientAndPrivilegedUsers(params, bot, code)
              )
              .then(() => _informIfStockLevelsAreLow(bot))
              .then(() => {
                resolve(
                  `Thanks for your input; a reward code has been sent to <@${recipient}>.`
                );
              })
              .catch(err => {
                reject({
                  type: "naughty",
                  text: "Bots don't need rewarding."
                });

                return;
              });
          })
          .catch(err => {
            console.log(err);

            reject({
              type: "sorry",
              text:
                "Problem finding how many rewards you've already given, sorry about that."
            });

            return;
          });

        // @TODO: Check that requester and recipient are part of Bet Tribe
      })
      .catch(err => {
        console.log(err);

        reject(err);
        return;
      });
  });
};

module.exports = giveReward;
