const commands = require("./commands");
module.exports = (bot, db) => {
  bot.rtm.on(bot.events.MESSAGE, message => {
    let ch = message.channel;
    let channelType = ch.slice(0, 1);
    if (channelType !== "D" || message.user === bot.id) return;

    // in the case of an edit, use embedded message object
    message = message.message || message;
    let text = message.text || null;
    let user = message.user || null;
    if (!text || !user) return;

    let command = commands.filter(cmd => cmd.pattern.test(text));

    if (
      !command.length ||
      command[0].action === "default" ||
      (command[0].privileged && bot.privilegedUsers.includes(user) === false)
    ) {
      if (!command.length) {
        console.log(`[Misunderstood]: ${text}`);
      }

      bot.rtm.sendMessage(
        `Hi.  I'm a bot, here to help you give little rewards to people you think deserve it.`,
        ch
      );
      bot.rtm.sendMessage(
        `Here are the things I understand, and how to get me to understand them.`,
        ch
      );
      for (cmd of commands.filter(cmd => {
        if (cmd.hidden) {
          return false;
        }

        if (cmd.privileged && bot.privilegedUsers.includes(user) === false) {
          return false;
        }

        return true;
      })) {
        bot.rtm.sendMessage(`${cmd.title}`, ch);
        bot.rtm.sendMessage(`eg. _${cmd.example}_`, ch);
      }

      return;
    }

    command = command[0];

    let params = [user, ...text.match(command.pattern).slice(1)];
    const action = require("./actions/" + command.action);

    action(params, bot, db)
      .then(msg => {
        if (msg instanceof Array) {
          msg.forEach(line => {
            bot.rtm.sendMessage(line, ch);
          });
        } else {
          bot.rtm.sendMessage(msg, ch);
        }
      })
      .catch(err => {
        const reactions = require("./reactions");
        if (reactions.hasOwnProperty(err.type)) {
          let ts = message.ts || message.message.ts;
          bot.web.reactions.add(reactions[err.type], {
            channel: message.channel,
            timestamp: ts
          });
        }
        bot.rtm.sendMessage(err.text, ch);

        // @TODO: Log rejections
        console.log(`[ERROR] ${err.text}`);
      });
  });
};
