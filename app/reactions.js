module.exports = {
  naughty: "fire",
  lacking: "confused",
  error: "exclamation",
  sorry: "cry"
};
