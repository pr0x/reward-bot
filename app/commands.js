module.exports = [
  {
    title: "Give a *reward* - you need to say to who (with their Slack username) and a reason.  If you want to thank more than one person, then you need to tell me for each of them separately: I can only give one reward at a time!",
    example: "Send a reward to @awesomeperson for going out of their way to help me performance test Super New Feature",
    pattern: /(?:send|give)?(?:.+)?reward(?:.+)<\@(\w+\b)>\ (.+)/i,
    action: "giveReward"
  },
  {
    title: "List all the rewards that everyone in the business has *given*",
    example: "List everything that people have given.",
    pattern: /(?:given)\b/i,
    action: "getAllRewards",
    privileged: true
  },
  {
    title: "Ask about *rewards* you've received or given before",
    example: "Which rewards have I received before?",
    pattern: /(?:awards|rewards)\b/i,
    action: "getIndividualRewards"
  },
  {
    pattern: /(?:hello|hi|hej)/i,
    action: "default",
    hidden: true
  }
];
