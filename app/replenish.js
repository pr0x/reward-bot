const MongoClient = require("mongodb").MongoClient;
const MongoUrl = "mongodb://mongo/" + require("./config").db;
const voucherFile = process.env.VOUCHER_FILE || "data/vouchers.txt";

console.log(`load vouchers from ${voucherFile}.`);
require("fs").readFile(voucherFile, "utf8", (err, data) => {
  if (err) throw err;
  data = data.split("\n").filter(line => line.length).map(line => {
    return { code: line };
  });

  MongoClient.connect(MongoUrl, (err, db) => {
    console.log("Connected correctly to Mongo Server");
    db.collection("vouchers").insertMany(data, { w: 1 }, (err, result) => {
      if (err) throw err;
      console.log(`${result.result.n} vouchers added.`);
      db.close();
      process.exit();
    });
  });
});
