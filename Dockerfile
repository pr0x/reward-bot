FROM node:alpine
RUN mkdir -p /usr/src/reward-bot

WORKDIR /usr/src/reward-bot

COPY package.json /usr/src/reward-bot
RUN npm install

COPY app /usr/src/reward-bot/app
COPY data /usr/src/reward-bot/data
ENTRYPOINT ["npm", "run"]
CMD ["start"]
